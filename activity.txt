1. List the books Authored by Marjorie Green.
	The Busy Executive's Database Guide
	You Can Combat Computer Stress!

2. List the books Authored by Michael O'Leary.
	Cooking with Computers
	Book with title id TC7777

3. Write the author/s of "The Busy Executive’s Database Guide".
	Marjorie Green

4. Identify the publisher of "But Is It User Friendly?".
	Algodata Infosystems

5. List the books published by Algodata Infosystems.
	But Is It User Friendly?
	Secrets of Silicon Valley
	Net Etiquette

